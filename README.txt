LinkedList Implementation for ECMA Script 6+;

Use:

    let LinkedList = require("../linkedList")

    let ll = new LinkedList();

Methods:

    //Insert at the beginning of the list
    insertAtStart(data)

	//Insert at the end of the list
	insertAtEnd(data)

	//Insert at specific place
	insertAtIndex(data, index)

	//Delete first node of list
	deleteFirst()

	//Delete last node of list
	deleteLast()

	//Delete at specific index
	deleteAt(index)

	//Clear list
	clear()

	//Get node at specific position
	getAt(index)

	//Get last node;
	getLast()

	getFirst()

	//Get list size
	getSize()

	//Get index of element
	indexOf(data)

    //Check if list contains item
    contains(data)

    //Iterate over list
    each(fn)
    Example: linkedList.each((el)=>console.log(el*3))



