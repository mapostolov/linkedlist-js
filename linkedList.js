"use strict"

class Node{
	constructor(data, next = null){
		this.data = data;
		this.next = next;
	}
}

class LinkedList{
	constructor(){
		this.head = null;
	}

	//Insert at the beginning of the list
	insertAtStart(data){
		let node = new Node(data);
		node.next = this.head;
		this.head = node;
	}

	//Insert at the end of the list
	insertAtEnd(data){
		let node = new Node(data);
		if(!this.head){
			this.head = node;
			return this.head;
		}

		//Travers to find the tail
		let t = this.head;
		while(t.next !== null){
			t = t.next;
		}
		t.next = node;
		return this.head;
	}

	//Insert at specific place
	insertAtIndex(data, index){
		//If the list is empty
		if(!this.head) return this.head = new Node(data);
		//If insert at the begging
		if(index === 0) return this.head = new Node(data, this.head);
		//If index out of bounds
        if (this._isOutOfBounds(index)){
            console.log("Index out of bounds!")
        	return
		}
		//Else
		const prev = this.getAt(index - 1);
		let node  = new Node(data);
		node.next = prev.next;
		prev.next = node;
		return this.head;
	}

	//Delete first node of list
	deleteFirst(){
		if(!this.head) return;
		this.head = this.head.next;
		return this.head;
	}

	//Delete last node of list
	deleteLast(){
		if(!this.head) return null;
		if(!this.head.next) return this.head = null;
		let prev = this.head;
		let t = this.head.next;

		while(t.next !== null){
			prev = t;
			t = t.next;
		}

		prev.next = null;
		return this.head;
	}

	//Delete at specific index
	deleteAt(index){
		if (!this.head) return this.head = new Node(data);
		if (index === 0) return this.head.next;
        if (this._isOutOfBounds(index)){
            console.log("Index out of bounds!")
            return
        }
        const prev = this.getAt(index - 1);
		if (!prev || !prev.next) return;
		prev.next = prev.next.next;
		return this.head;
	}

	//Delete list
	clear(){
		this.head = null;
	}


	//Get node at specific position
	getAt(index){
		let c = 0;
		let node = this.head;
		while (node){
			if (c === index){
				return node;
			}
			c++;
			node = node.next;
		}
		return null;
	}

	//Get last node;
	getLast(){
		let lNode = this.head;
		if(lNode){
			while (lNode.next){
				lNode = lNode.next;
			}
		}

		return lNode;
	}

	getFirst(){
		return this.head;
	}

	//Get list size
	getSize(){
		let c = 0;
		let node = this.head;
		while(node){
			c++;
			node = node.next;
		}
		return c;
	}


	//Get index of Item
	indexOf(data){
		let c = 0;
		let node = this.head;
		if(node){
			while (node.next){
				if(node.data === data) return c;
				c++;
                node = node.next;
			}
		return -1;
		}
	}

    //Get index of Item
    contains(data){
        let node = this.head;
        if(node){
            while (node.next){
                if(node.data === data) return true;
                node = node.next;
            }
            return false;
        }
    }

    each(fn){
		let node = this.head;
		if(node){
			while (node){
				fn.call(this, node.data);
				if(!node.next) return;
				node = node.next;
			}
		}
	}

	_isOutOfBounds(index){
        let listSize = this.getSize();
        return index > listSize-1 || index < 0;
	}

}


module.exports = LinkedList;
