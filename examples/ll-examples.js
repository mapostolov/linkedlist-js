let LinkedList = require("../linkedList")
let ll = new LinkedList();

console.log(`Init new LinkedList ${JSON.stringify(ll)}`);
ll.insertAtStart(10);
console.log(`Added value at the beginning ${JSON.stringify(ll)}`);
ll.insertAtStart(5);
console.log(`Added value at the beginning ${JSON.stringify(ll)}`);
ll.insertAtEnd(20);
console.log(`Added value at the End ${JSON.stringify(ll)}`);
ll.insertAtIndex(15, 2);
console.log(`Added value at the Index ${JSON.stringify(ll)}`);
//Try to insert out of bounds
ll.insertAtIndex(15, 222);
ll.insertAtIndex(15, -1);
console.log(`Index of 15 is ${ll.indexOf(15)}`);
console.log(`Contains 15 ${ll.contains(15)}`);
console.log(`Contains 55 ${ll.contains(55)}`);
console.log(`Get size of Linked list ${ll.getSize()}`);
ll.each((el)=>console.log(el*3))
console.log(`Get First object ${JSON.stringify(ll.getFirst())}`);
console.log(`Get Last object ${JSON.stringify(ll.getLast())}`);
console.log(`Get At index ${JSON.stringify(ll.getAt(2))}`);
console.log(`LL before delete at index ${JSON.stringify(ll)}`);
ll.deleteAt(2);
console.log(`LL after delete at index ${JSON.stringify(ll)}`);
console.log(`LL before delete first ${JSON.stringify(ll)}`);
ll.deleteFirst();
console.log(`LL after delete first ${JSON.stringify(ll)}`);
console.log(`LL before delete last ${JSON.stringify(ll)}`);
ll.deleteLast();
console.log(`LL after delete last ${JSON.stringify(ll)}`);
ll.clear();
console.log(`After Linked list cleared ${JSON.stringify(ll)}`);